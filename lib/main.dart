import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'blocs/authentication/authentication.dart';
import 'blocs/authentication/authentication_bloc.dart';
import 'blocs/login/login_bloc.dart';
import 'blocs/loginform_validation/loginform_validation_bloc.dart';
import 'blocs/navigator/navigation_bloc.dart';
import 'blocs/register/register_bloc.dart';
import 'blocs/registerform_validation/registerform_validation_bloc.dart';
import 'blocs/users/users_bloc.dart';
import 'config/register_routes.dart';
import 'ui/values/values.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  final GlobalKey<NavigatorState> _navigatorKey = GlobalKey();

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.light.copyWith(
      statusBarColor: Colors.transparent, //or set color with: Color(0xFF0000FF)
    ));
    WidgetsFlutterBinding.ensureInitialized();

    return MultiBlocProvider(
      providers: [
        BlocProvider<NavigationBloc>(
          create: (context) => NavigationBloc(navigatorKey: _navigatorKey),
        ),
        BlocProvider<AuthenticationBloc>(
          create: (context) => AuthenticationBloc(
              userRepository: userRepository,
              navigationBloc: BlocProvider.of<NavigationBloc>(context))
            ..add((AppStarted())),
        ),
        BlocProvider(
          create: (context) => LoginBloc(
            authenticationBloc: BlocProvider.of<AuthenticationBloc>(context),
            userRepository: userRepository,
          ),
        ),
        BlocProvider(
          create: (context) => LoginformValidationBloc()..add(FormReset()),
        ),
        BlocProvider(
          create: (context) => RegisterBloc(
            authenticationBloc: BlocProvider.of<AuthenticationBloc>(context),
            userRepository: userRepository,
          ),
        ),
        BlocProvider(
          create: (context) => RegisterformValidationBloc(),
        ),
        BlocProvider<UsersBloc>(
          create: (context) =>
              UsersBloc(userRepository: userRepository)
                ..add(GetListUsers()),
        ),
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        navigatorKey: _navigatorKey,
        title: StringConst.APP_NAME,
        theme: ThemeData(
          primarySwatch: Colors.blue,
          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
        initialRoute: '/',
        routes: mapRegisterRoutes(),
        onGenerateRoute: mapRegisterRoutesWithParameters,
      ),
    );
  }
}
