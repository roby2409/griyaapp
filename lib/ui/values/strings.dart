part of values;

class StringConst {
  //strings
  static const String APP_NAME = "Griya App";
  static const String TUAN_KENTANG = "Griya Tuan Kentang";





  // string untuk login dan register
  static const String FORGOT_PASSWORD_QUESTION = "Lupa Password?";
  static const String HAVE_AN_ACCOUNT_QUESTION = "Sudah punya akun?";
  static const String FORGOT_PASSWORD = "Lupa Password";
  static const String RESET_PASSWORD_DESCRIPTION =
      "Masukkan email Anda dan kami akan mengirimkan petunjuk tentang cara meresetnya";

  static const String HINT_TEXT_NAME = "Nama";
  static const String HINT_TEXT_EMAIL = "Email";
  static const String HINT_TEXT_PASSWORD = "Password";
  static const String HINT_TEXT_CONFIRM_PASSWORD = "Konfirmasi Password";
  static const String HINT_TEXT_HOME_SEARCH_BAR = "Temukan Restaurants";
  static const String HINT_TEXT_TRENDING_SEARCH_BAR = "Cari";
  static const String LOGIN = "Login";
  static const String SKIP = "Skip";
  static const String REGISTER = "Register";
  static const String CREATE_NEW_ACCOUNT = "Buat akun baru";


  // string untuk splash
  static const String titleSplash = "Pilih Item";
  static const String titleSplash2 = "Pilih Item";
  static const String titleSplash3 = "Pilih Item";


  static const String deskripsiSplash = "Pilih Item di mana pun Anda berada dengan aplikasi ini untuk membuat hidup lebih mudah";
  static const String deskripsiSplash2 = "Berbelanja dari ribuan semuaProvinsi di dunia \ n dalam satu aplikasi dengan harga yang sangat murah";


  //Font Family
  static const String FONT_FAMILY = "Josefin Sans";
}
