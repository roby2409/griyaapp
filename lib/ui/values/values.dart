library values;

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

part 'sizes.dart';
part 'strings.dart';
part 'styles.dart';
part 'colors.dart';
part 'decorations.dart';
part 'shadows.dart';
part 'gradients.dart';
part 'images.dart';