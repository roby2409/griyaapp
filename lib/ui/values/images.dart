part of values;

class ImagePath {
  //images route
  static const String imageDir = "assets";

  // splash
  static const String splashImage = "$imageDir/ilustration/ilustrasionboarding.png";
  static const String splashImage2 = "$imageDir/ilustration/ilustrasionboarding2.png";
  static const String splashImage3 = "$imageDir/ilustration/ilustrasionboarding3.png";

  // image
  static const String griyafoto = "$imageDir/images/griyafoto.jpg";

  // icons
  static const String emailIcon = "$imageDir/icons/email_icon.png";
  static const String passwordIcon = "$imageDir/icons/password_icon.png";
  static const String personIconMedium = "$imageDir/icons/person_icon_md.png";
  static const String uploadIcon = "$imageDir/icons/upload_icon.png";
  static const String personIcon = "$imageDir/icons/person_icon.png";
}
