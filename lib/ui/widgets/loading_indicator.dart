part of widgets;
class LoadingIndicator extends StatefulWidget {
  LoadingIndicator({Key key, this.title, this.color = Colors.blue})
      : super(key: key);

  final String title;
  final Color color;

  @override
  _LoadingIndicatorState createState() => _LoadingIndicatorState();
}

class _LoadingIndicatorState extends State<LoadingIndicator>
    with TickerProviderStateMixin {
  AnimationController _controller;

  @override
  void initState() {
    _controller = AnimationController(
      duration: const Duration(milliseconds: 5000),
      vsync: this,
    );

    _controller.addListener(() {
      if (_controller.isCompleted) {
        _controller.repeat();
      }
    });

    super.initState();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    _controller.forward();
    return Scaffold(
      backgroundColor: Colors.white,
      body: Stack(
        alignment: FractionalOffset.center,
        children: [
          Dialog(
            child: Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                RotationTransition(
                  turns: Tween(begin: 0.0, end: 1.0).animate(_controller),
                  child: Icon(
                    Icons.loop,
                    size: 40,
                    color: widget.color,
                  ),
                ),
                new Text("Loading"),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
