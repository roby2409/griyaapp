part of widgets;

class BuildListDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      scrollDirection: Axis.vertical,
      child: BlocBuilder<AuthenticationBloc, AuthenticationState>(
        builder: (context, state) {
          return Column(children: <Widget>[
            new UserAccountsDrawerHeader(
              accountName: Container(),
              accountEmail: Container(),
              currentAccountPicture: GestureDetector(
                child: new CircleAvatar(
                  backgroundColor: Colors.white,
                  child: Icon(
                    Icons.person,
                    color: Colors.blue,
                  ),
                ),
              ),
              decoration: new BoxDecoration(color: Colors.lightBlue.shade900),
            ),

//            body

            InkWell(
              onTap: () {
                BlocProvider.of<NavigationBloc>(context)
                    .add(NavigateToHomeScreenEvent());
              },
              child: ListTile(
                title: Text('Beranda'),
                leading: Icon(
                  Icons.home,
                  color: Colors.lightBlue,
                ),
              ),
            ),

            InkWell(
              onTap: () {
                BlocProvider.of<NavigationBloc>(context)
                    .add(NavigateToHomeScreenEvent());
              },
              child: ListTile(
                title: Text('Akun'),
                leading: Icon(
                  Icons.person,
                  color: Colors.purple,
                ),
              ),
            ),
            Divider(),

            InkWell(
              onTap: () {
                (state is AuthenticationAuthenticated)
                    ? BlocProvider.of<AuthenticationBloc>(context).add(LoggedOut())
                    : BlocProvider.of<NavigationBloc>(context)
                        .add(NavigateToLoginEvent());
              },
              child: ListTile(
                title: (state is AuthenticationUnauthenticated)
                    ? Text('Log In')
                    : Text('Log Out'),
                leading: Icon(
                  Icons.arrow_right,
                  color: Colors.red,
                ),
              ),
            ),
          ]);
        },
      ),
    );
  }
}
