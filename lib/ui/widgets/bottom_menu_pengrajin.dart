part of widgets;

class BottomMenuPengrajin extends StatelessWidget {
  final int menuIndex;

  BottomMenuPengrajin(this.menuIndex);

  Color colorByIndex(ThemeData theme, int index) {
    return index == menuIndex ? Colors.red : Colors.lightBlue;
  }

  BottomNavigationBarItem getItem(
      IconData icon, String title, ThemeData theme, int index) {
    return BottomNavigationBarItem(
      icon:  Icon(
        icon,
        color: colorByIndex(theme, index),
      ),
      title: Text(
        title,
        style: TextStyle(
          fontSize: 10.0,
          color: colorByIndex(theme, index),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final _theme = Theme.of(context);
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.only(
            topRight: Radius.circular(15), topLeft: Radius.circular(15)),
        boxShadow: [
          BoxShadow(color: Colors.black38, spreadRadius: 0, blurRadius: 10),
        ],
      ),
      child: ClipRRect(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(15.0),
          topRight: Radius.circular(15.0),
        ),
        child: BottomNavigationBar(
          type: BottomNavigationBarType.fixed,
          currentIndex: menuIndex,
          onTap: (value) {
            switch (value) {
              case 0:
                // BlocProvider.of<NavigationBloc>(context)
                //     .add(NavigateToHomeScreenEvent());
                break;
              case 1:
                BlocProvider.of<NavigationBloc>(context)
                    .add(NavigateToHomeScreenEvent());
                break;
              case 2:
                // BlocProvider.of<NavigationBloc>(context)
                //     .add(NavigateToAktifitasEvent());
                break;
            }
          },
          items: [
            getItem(Icons.trending_up, 'Pemasukan', _theme, 0),
            getItem(Icons.home, 'Home', _theme, 1),
            getItem(Icons.trending_down, 'Pengeluaran', _theme, 2),
          ],
        ),
      ),
    );
  }
}