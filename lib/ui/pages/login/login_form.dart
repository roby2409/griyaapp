import 'package:griya/blocs/login/login_bloc.dart';
import 'package:griya/blocs/loginform_validation/loginform_validation_bloc.dart';
import 'package:griya/ui/pages/login/login_footer.dart';
import 'package:griya/ui/values/values.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:griya/ui/widgets/widgets.dart';

class LoginForm extends StatefulWidget {
  @override
  State<LoginForm> createState() => _LoginFormState();
}

class _LoginFormState extends State<LoginForm> {
  final _emailController = TextEditingController();
  final _passwordController = TextEditingController();
  var tap = 0;
  bool get isPopulated =>
      _emailController.text.isNotEmpty && _passwordController.text.isNotEmpty;

  bool isLoginButtonEnabled(
      LoginState state, LoginformValidationState stateValidation) {
    return stateValidation.isValidForm && isPopulated && state is! LoginLoading;
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<LoginBloc, LoginState>(
      listener: (context, state) {
        if (state is LoginFailure) {
          Scaffold.of(context)
            ..hideCurrentSnackBar()
            ..showSnackBar(
              SnackBar(
                content: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [Text(state.error), Icon(Icons.error)],
                ),
                backgroundColor: Colors.red,
              ),
            );
        }
      },
      child: BlocBuilder<LoginBloc, LoginState>(
        builder: (context, state) {
          return BlocBuilder<LoginformValidationBloc, LoginformValidationState>(
              builder: (context, stateValidation) {
            return Container(
              margin: const EdgeInsets.symmetric(horizontal: Sizes.MARGIN_48),
              child: Column(
                children: [
                  CustomTextFormField(
                      controller: _emailController,
                      onChanged: (String currentValue) {
                        BlocProvider.of<LoginformValidationBloc>(context)
                            .add(EmailChanged(email: currentValue));
                      },
                      hasPrefixIcon: true,
                      prefixIconImagePath: ImagePath.emailIcon,
                      hintText: StringConst.HINT_TEXT_EMAIL,
                      obscured: false,
                      errorText: stateValidation.isInProcessEmail
                          ? 'Validasi input...'
                          : stateValidation.errorEmailMessage),
                  SpaceH16(),
                  CustomTextFormField(
                      controller: _passwordController,
                      onChanged: (String currentValue) {
                        BlocProvider.of<LoginformValidationBloc>(context)
                            .add(PasswordChanged(password: currentValue));
                      },
                      hasPrefixIcon: true,
                      prefixIconImagePath: ImagePath.passwordIcon,
                      hintText: StringConst.HINT_TEXT_PASSWORD,
                      obscured: true,
                      errorText: stateValidation.isInProcessPassword
                          ? 'Validasi input...'
                          : stateValidation.errorPasswordMessage),
                  Container(
                    child: state is LoginLoading
                        ? Column(
                          children: <Widget>[
                            SpaceH16(),
                            CircularProgressIndicator(),
                          ],
                        )
                        : null,
                  ),
                  isLoginButtonEnabled(state, stateValidation)
                      ? LoginFooter(
                          buttonLogin:
                              ButtonCustom(StringConst.LOGIN, onTap: () {
                            BlocProvider.of<LoginBloc>(context).add(
                              LoginButtonPressed(
                                email: _emailController.text,
                                password: _passwordController.text,
                              ),
                            );
                          }),
                        )
                      : LoginFooter(
                          buttonLogin: ButtonCustom(StringConst.LOGIN,
                              decoration: Decorations.defaultButtonDecoration,
                              buttonTextStyle: Styles.customNormalTextStyle(
                                  color: AppColors.primaryText),
                              onTap: () {}),
                        ),
                ],
              ),
            );
          });
        },
      ),
    );
  }
}
