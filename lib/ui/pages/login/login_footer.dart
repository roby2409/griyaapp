import 'package:griya/blocs/login/login_bloc.dart';
import 'package:griya/blocs/loginform_validation/loginform_validation_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:griya/blocs/navigator/navigation_bloc.dart';
import 'package:griya/ui/values/values.dart';
import 'package:griya/ui/widgets/widgets.dart';

class LoginFooter extends StatelessWidget {
  final Widget buttonLogin;

  LoginFooter({Key key, @required this.buttonLogin})
      : assert(buttonLogin != null),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<LoginBloc, LoginState>(builder: (context, state) {
      return BlocBuilder<LoginformValidationBloc, LoginformValidationState>(
        builder: (context, stateValidation) {
          return Column(
            children: <Widget>[
              SpaceH30(),
              buttonLogin,
              SizedBox(height: Sizes.HEIGHT_50),
              InkWell(
                onTap: () {
                  BlocProvider.of<NavigationBloc>(context)
                    .add(NavigateToRegisterEvent());
                },
                child: Container(
                  width: Sizes.WIDTH_150,
                  height: Sizes.HEIGHT_24,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Text(
                        StringConst.CREATE_NEW_ACCOUNT,
                        textAlign: TextAlign.center,
                        style: Styles.customNormalTextStyle(),
                      ),
                      Spacer(),
                      Container(
                        height: 5,
                        margin: EdgeInsets.symmetric(horizontal: 1),
                        decoration: Decorations.horizontalBarDecoration,
                        child: Container(
                          color: Colors.blue,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          );
        },
      );
    });
  }
}
