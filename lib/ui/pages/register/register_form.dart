import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:griya/blocs/navigator/navigation_bloc.dart';
import 'package:griya/blocs/register/register_bloc.dart';
import 'package:griya/ui/pages/register/register_footer.dart';
import 'package:griya/ui/values/values.dart';
import 'package:griya/ui/widgets/widgets.dart';
import 'package:griya/blocs/registerform_validation/registerform_validation_bloc.dart';

class RegisterForm extends StatefulWidget {
  @override
  _RegisterFormState createState() => _RegisterFormState();
}

class _RegisterFormState extends State<RegisterForm> {
  final _namaController = TextEditingController();
  final _emailController = TextEditingController();
  final _passwordController = TextEditingController();
  final _confirmPasswordController = TextEditingController();

  
  var tap = 0;
  bool get isPopulated =>
      _namaController.text.isNotEmpty &&
      _emailController.text.isNotEmpty &&
      _passwordController.text.isNotEmpty;

  bool isRegisterButtonEnabled(
      RegisterState state, RegisterformValidationState stateValidation) {
    return stateValidation.isValidForm &&
        isPopulated &&
        state is! RegisterLoading;
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<RegisterBloc, RegisterState>(
        listener: (context, state) {
      if (state is RegisterFailure) {
        Scaffold.of(context)
          ..hideCurrentSnackBar()
          ..showSnackBar(
            SnackBar(
              content: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [Text(state.error), Icon(Icons.error)],
              ),
              backgroundColor: Colors.red,
            ),
          );
      }
    }, child:
            BlocBuilder<RegisterBloc, RegisterState>(builder: (context, state) {
      return BlocBuilder<RegisterformValidationBloc,
          RegisterformValidationState>(builder: (context, stateValidation) {
        return Column(
          children: <Widget>[
            CustomTextFormField(
                controller: _namaController,
                onChanged: (String currentValue) {
                  BlocProvider.of<RegisterformValidationBloc>(context)
                      .add(NamaChanged(nama: currentValue));
                },
                hasPrefixIcon: true,
                prefixIconImagePath: ImagePath.personIcon,
                hintText: StringConst.HINT_TEXT_NAME,
                errorText: stateValidation.isInProcessNama
                    ? 'Validasi input...'
                    : stateValidation.errorNamaMessage),
            SpaceH16(),
            CustomTextFormField(
                controller: _emailController,
                onChanged: (String currentValue) {
                  BlocProvider.of<RegisterformValidationBloc>(context)
                      .add(EmailChanged(email: currentValue));
                },
                hasPrefixIcon: true,
                prefixIconImagePath: ImagePath.emailIcon,
                hintText: StringConst.HINT_TEXT_EMAIL,
                errorText: stateValidation.isInProcessEmail
                    ? 'Validasi input...'
                    : stateValidation.errorEmailMessage),
            SpaceH16(),
            CustomTextFormField(
              hasPrefixIcon: true,
              prefixIconImagePath: ImagePath.passwordIcon,
              hintText: StringConst.HINT_TEXT_PASSWORD,
              obscured: true,
            ),
            SpaceH16(),
            CustomTextFormField(
              hasPrefixIcon: true,
              prefixIconImagePath: ImagePath.passwordIcon,
              hintText: StringConst.HINT_TEXT_CONFIRM_PASSWORD,
              obscured: true,
            ),
            Container(
              child: state is RegisterLoading
                  ? Column(
                      children: <Widget>[
                        SpaceH16(),
                        CircularProgressIndicator(),
                      ],
                    )
                  : null,
            ),
            SpaceH16(),
            isRegisterButtonEnabled(state, stateValidation)
                ? RegisterFooter(
                    buttonRegister:
                        ButtonCustom(StringConst.REGISTER, onTap: () {
                      BlocProvider.of<RegisterBloc>(context).add(
                        RegisterButtonPressed(
                          email: _emailController.text,
                          password: _passwordController.text,
                        ),
                      );
                    }),
                  )
                : RegisterFooter(
                    buttonRegister: ButtonCustom(StringConst.REGISTER,
                        decoration: Decorations.defaultButtonDecoration,
                        buttonTextStyle: Styles.customNormalTextStyle(
                            color: AppColors.primaryText),
                        onTap: () {}),
                  )
          ],
        );
      });
    }));
  }
}
