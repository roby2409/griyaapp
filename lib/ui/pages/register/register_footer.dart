import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:griya/blocs/navigator/navigation_bloc.dart';
import 'package:griya/blocs/register/register_bloc.dart';
import 'package:griya/blocs/registerform_validation/registerform_validation_bloc.dart';
import 'package:griya/ui/values/values.dart';
import 'package:griya/ui/widgets/widgets.dart';

class RegisterFooter extends StatelessWidget {
  final Widget buttonRegister;

  RegisterFooter({Key key, @required this.buttonRegister})
      : assert(buttonRegister != null),
        super(key: key);
        
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<RegisterBloc, RegisterState>(builder: (context, state) {
      return BlocBuilder<RegisterformValidationBloc,
          RegisterformValidationState>(
        builder: (context, stateValidation) {
          return Column(
            children: <Widget>[
              SpaceH40(),
              buttonRegister,
              SpaceH40(),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    StringConst.HAVE_AN_ACCOUNT_QUESTION,
                    textAlign: TextAlign.right,
                    style: Styles.customNormalTextStyle(),
                  ),
                  SpaceW16(),
                  InkWell(
                    onTap: () {
                      BlocProvider.of<NavigationBloc>(context)
                          .add(NavigateToLoginEvent());
                    },
                    child: Text(
                      StringConst.LOGIN,
                      textAlign: TextAlign.left,
                      style: Styles.customNormalTextStyle(
                        color: AppColors.secondaryElement,
                      ),
                    ),
                  ),
                  SpaceW36(),
                ],
              ),
            ],
          );
        },
      );
    });
  }
}
