import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:griya/blocs/authentication/authentication.dart';
import 'package:griya/repositories/user_repository.dart';
import 'package:griya/ui/pages/admin/home.dart';
import 'package:griya/ui/pages/admin/kategori/daftar_kategori.dart';
import 'package:griya/ui/pages/admin/user/daftar_user.dart';
import 'package:griya/ui/pages/login/login_page.dart';
import 'package:griya/ui/pages/onboarding/on_boarding.dart';
import 'package:griya/ui/pages/pengrajin/home.dart';
import 'package:griya/ui/pages/register/register_page.dart';
import 'package:griya/ui/pages/splash/splash.dart';
import 'package:griya/ui/widgets/widgets.dart';
import 'routes.dart';

final UserRepository userRepository = UserRepository();
Map<String, WidgetBuilder> mapRegisterRoutes() {
  return <String, WidgetBuilder>{
    OpenGriyaRoutes.splashscreen: (context) => SplashPage(),
    OpenGriyaRoutes.onboarding: (context) => OnBoarding(),
    OpenGriyaRoutes.register: (context) => RegisterPage(),
    OpenGriyaRoutes.appscreen: (context) =>
        BlocBuilder<AuthenticationBloc, AuthenticationState>(
          builder: (context, state) {
            if (state is AuthenticationUninitialized) {
              return LoadingIndicator();
            } else if (state is AuthenticationAuthenticated) {
              if (state.user.level == 1) {
                print(state.user.level);
                return blocBuilderPage(HomeAdmin(
                  title: 'Home Admin',
                ));
              } else {
                return blocBuilderPage(HomePengrajin(
                  title: 'Home Pengrajin',
                ));
              }
            } else if (state is AuthenticationUnauthenticated) {
              return LoginPage(userRepository: userRepository);
            } else if (state is AuthenticationLoading) {
              return LoadingIndicator();
            }
            return Container();
          },
        ),
    OpenGriyaRoutes.daftarusers: (context) => blocBuilderPage(DaftarUser(),),
    OpenGriyaRoutes.daftarkategori: (context) => blocBuilderPage(DaftarKategori(),)
  };
}

BlocBuilder<AuthenticationBloc, AuthenticationState> blocBuilderPage(
    Widget page) {
  return BlocBuilder<AuthenticationBloc, AuthenticationState>(
    builder: (context, state) {
      if (state is AuthenticationAuthenticated) {
        return page;
      }
      if (state is AuthenticationUnauthenticated) {
        return LoginPage(userRepository: userRepository);
      }
      if (state is AuthenticationLoading) {
        return LoadingIndicator();
      }
      return LoadingIndicator();
    },
  );
}

Route mapRegisterRoutesWithParameters(RouteSettings settings) {
  Route page;
  switch (settings.name) {
    case OpenGriyaRoutes.login:
      page = PageRouteBuilder(
        pageBuilder: (_, __, ___) => LoginPage(
          userRepository: userRepository,
        ),
        transitionsBuilder:
            (_, Animation<double> animation, __, Widget widget) {
          return Opacity(
            opacity: animation.value,
            child: widget,
          );
        },
        transitionDuration: Duration(milliseconds: 1500),
      );
      break;
  }
  return page;
}
