import 'dart:async';

import 'package:griya/blocs/authentication/authentication.dart';
import 'package:meta/meta.dart';
import 'package:equatable/equatable.dart';
import 'package:bloc/bloc.dart';
import 'package:griya/repositories/user_repository.dart';

part 'register_event.dart';
part 'register_state.dart';

class RegisterBloc extends Bloc<RegisterEvent, RegisterState> {
  final UserRepository userRepository;
  final AuthenticationBloc authenticationBloc;

  RegisterBloc({
    @required this.userRepository,
    @required this.authenticationBloc,
  })  : assert(userRepository != null),
        assert(authenticationBloc != null);

  RegisterState get initialState => RegisterInitial();

  @override
  Stream<RegisterState> mapEventToState(RegisterEvent event) async* {
    if (event is RegisterButtonPressed) {
      yield RegisterLoading();

      try {
        final user = await userRepository.authenticate(
          email: event.email,
          password: event.password,
        );
        authenticationBloc.add(LoggedIn(user: user));
        yield RegisterInitial();
      } catch (error) {
        print(error.toString());
        yield RegisterFailure(error: error.toString());
      }
    }
  }
}
