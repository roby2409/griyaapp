part of 'users_bloc.dart';

@immutable
abstract class UsersState extends Equatable {
  @override
  List<Object> get props => [];
  UsersState([List props = const []]);
}

class UsersInitial extends UsersState {}

class UsersError extends UsersState {
  final String errorMessage;
  UsersError({
    this.errorMessage,
  });
}

// ini state kategori berhasil di muat
class UsersWaiting extends UsersState {
  final String pesanLoading;
  UsersWaiting({this.pesanLoading});
}

class UsersCompleted extends UsersState {
  final List<User> users;

  UsersCompleted({@required this.users}) : super([users]);
}
