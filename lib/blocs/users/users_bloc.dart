import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:griya/models/user.dart';
import 'package:griya/repositories/user_repository.dart';
import 'package:meta/meta.dart';

part 'users_event.dart';
part 'users_state.dart';

class UsersBloc extends Bloc<UsersEvent, UsersState> {
  final UserRepository userRepository;
  

  UsersBloc({
    this.userRepository,
  }) : assert(userRepository != null);

  @override
  UsersState get initialState => UsersInitial();

  @override
  Stream<UsersState> mapEventToState(
    UsersEvent event,
  ) async* {
    if (event is GetListUsers) {
      yield UsersWaiting(pesanLoading: "Memuat data user");
      try {
        final semuaUser = await userRepository.fetchUserList();
        yield UsersCompleted(users: semuaUser);
      } catch (error) {
        yield UsersError(errorMessage: error.toString());
      }
    }
  }
}
