import 'dart:async';

import 'package:griya/blocs/navigator/navigation_bloc.dart';
import 'package:griya/repositories/user_repository.dart';
import 'package:meta/meta.dart';
import 'package:bloc/bloc.dart';

import 'authentication.dart';

class AuthenticationBloc
    extends Bloc<AuthenticationEvent, AuthenticationState> {
  final UserRepository userRepository;
  final NavigationBloc navigationBloc;

  AuthenticationBloc(
      {@required this.userRepository, @required this.navigationBloc})
      : assert(userRepository != null),
        assert(navigationBloc != null);

  @override
  AuthenticationState get initialState => AuthenticationUninitialized();

  @override
  Stream<AuthenticationState> mapEventToState(
    AuthenticationEvent event,
  ) async* {
    if (event is AppStarted) {
      // ketika event appstarted
      print('okeee');
      UserRepository userRepository = UserRepository();
      final bool hasToken =
          await userRepository.hasToken(); // cek apo dio punyo token
      if (hasToken) {
        final user = await userRepository.getUser();
        try {
          final checkUserNotExpired =
              await userRepository.notExpired(user.token);
              yield AuthenticationAuthenticated(user: checkUserNotExpired);
        } catch (error) {
          print("belum di autentikasi");
          yield AuthenticationUnauthenticated(); // belum terautentikasi
        }
      } else {
        print("belum di autentikasi");
        yield AuthenticationUnauthenticated(); // belum terautentikasi
      }
    }

    if (event is LoggedIn) {
      // ketika event login
      yield AuthenticationLoading(); // loading authentication
      print(event.user);
      UserRepository userRepository = UserRepository();
      await userRepository.persistToken(user: event.user); // pertahankan token
      final user = await userRepository.getUser();
      yield AuthenticationAuthenticated(user: user);
    }

    if (event is LoggedOut) {
      yield AuthenticationLoading();
      UserRepository userRepository = UserRepository();
      await userRepository.deleteToken(id: 0);
      navigationBloc.add(NavigateToHomeScreenEvent());
      yield AuthenticationUnauthenticated();
    }
  }
}
