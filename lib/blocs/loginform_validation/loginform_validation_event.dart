part of 'loginform_validation_bloc.dart';

@immutable
abstract class LoginformValidationEvent extends Equatable{
  const LoginformValidationEvent();

  @override
  List<Object> get props => [];
}
class ValidateInput extends LoginformValidationEvent {
  final String input;

  ValidateInput({@required this.input});
}

class EmailChanged extends LoginformValidationEvent {
  final String email;

  const EmailChanged({@required this.email});

  @override
  List<Object> get props => [email];

  @override
  String toString() => 'EmailChanged { email: $email }';
}

class PasswordChanged extends LoginformValidationEvent {
  final String password;

  const PasswordChanged({@required this.password});

  @override
  List<Object> get props => [password];

  @override
  String toString() => 'PasswordChanged { password: $password }';
}


class FormReset extends LoginformValidationEvent {}