part of 'loginform_validation_bloc.dart';

@immutable
class LoginformValidationState {
  final bool isInProcessEmail;
  final bool isInProcessPassword;

  final bool isEmailValidated;
  final bool isPasswordValidated;

  final String errorEmailMessage;
  final String errorPasswordMessage;

  bool get isValidForm =>
      errorEmailMessage == null && errorPasswordMessage == null;

  LoginformValidationState(
      {this.isInProcessEmail,
      this.isInProcessPassword,

      this.isEmailValidated,
      this.isPasswordValidated,

      this.errorEmailMessage,
      this.errorPasswordMessage});

  factory LoginformValidationState.empty() {
    return LoginformValidationState(
        isInProcessEmail: false,
        isInProcessPassword: false,

        isEmailValidated: false,
        isPasswordValidated: false);
  }

  LoginformValidationState copyWith(
      {bool isInProcessEmail,
      bool isInProcessPassword,

      bool isEmailValidated,
      bool isPasswordValidated,

      String errorEmailMessage,
      String errorPasswordMessage}) {
    return LoginformValidationState(
      isEmailValidated: isEmailValidated ?? this.isEmailValidated,
      isPasswordValidated: isPasswordValidated ?? this.isPasswordValidated,

      isInProcessEmail: isInProcessEmail ?? this.isInProcessEmail,
      isInProcessPassword: isInProcessPassword ?? this.isInProcessPassword,

      errorEmailMessage: errorEmailMessage,
      errorPasswordMessage: errorPasswordMessage,
    );
  }

  LoginformValidationState loadingEmail() {
    return this.copyWith(isInProcessEmail: true);
  }

  LoginformValidationState loadingPassword() {
    return this.copyWith(isInProcessPassword: true);
  }

  LoginformValidationState validatedEmail(
      {@required String errorEmailMessage}) {
    return this.copyWith(
        errorEmailMessage: errorEmailMessage, isInProcessEmail: false);
  }

  LoginformValidationState validatedPassword(
      {@required String errorPasswordMessage}) {
    return this.copyWith(
        errorPasswordMessage: errorPasswordMessage, isInProcessPassword: false);
  }
}
