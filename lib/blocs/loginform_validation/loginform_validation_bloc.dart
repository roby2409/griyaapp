import 'dart:async';
import 'package:griya/common/app_validator.dart';
import 'package:rxdart/rxdart.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

part 'loginform_validation_event.dart';
part 'loginform_validation_state.dart';

class LoginformValidationBloc
    extends Bloc<LoginformValidationEvent, LoginformValidationState> {
  @override
  LoginformValidationState get initialState => LoginformValidationState.empty();

  @override
  Stream<Transition<LoginformValidationEvent, LoginformValidationState>> transformEvents(
    Stream<LoginformValidationEvent> events,
    TransitionFunction<LoginformValidationEvent, LoginformValidationState> transitionFn,
  ) {
    final nonDebounceStream = events.where((event) {
      return (event is! EmailChanged && event is! PasswordChanged);
    });
    final debounceStream = events.where((event) {
      return (event is EmailChanged || event is PasswordChanged);
    }).debounceTime(Duration(milliseconds: 300));
    return super.transformEvents(
      nonDebounceStream.mergeWith([debounceStream]),
      transitionFn,
    );
  }

  @override
  Stream<LoginformValidationState> mapEventToState(
    LoginformValidationEvent event,
  ) async* {
    if (event is EmailChanged) {
      yield this.state.loadingEmail();
      String backendValidationMessage = await this.simulatedBackendEmailFunctionality(event.email);
      yield this.state.validatedEmail(errorEmailMessage: backendValidationMessage);
    }
    if (event is PasswordChanged) {
      yield this.state.loadingPassword();
      String backendValidationMessage = await this.simulatedBackendPasswordFunctionality(event.password);
      yield this.state.validatedPassword(errorPasswordMessage: backendValidationMessage);
    }
    if (event is FormReset) {
      yield LoginformValidationState.empty();
    }
  }

  Future<String> simulatedBackendEmailFunctionality(String input) async {
    // This simulates delay of the backend call
    await Future.delayed(Duration(milliseconds: 500));
    // This simulates the return of the backend call
    return Validators.validateEmail(input);
  }


  Future<String> simulatedBackendPasswordFunctionality(String input) async {
    // This simulates delay of the backend call
    await Future.delayed(Duration(milliseconds: 500));
    // This simulates the return of the backend call
    return Validators.valueExists(input);
  }
}
