import 'dart:async';
import 'package:griya/repositories/user_repository.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:meta/meta.dart';
import 'package:griya/config/routes.dart';

part 'navigation_event.dart';
part 'navigation_state.dart';

class NavigationBloc extends Bloc<NavigationEvent, dynamic> {
  final GlobalKey<NavigatorState> navigatorKey;
  final UserRepository userRepository;
  NavigationBloc({
    @required this.navigatorKey,
    this.userRepository,
  }) : assert(navigatorKey != null);

  @override
  dynamic get initialState => 0;

  @override
  Stream<dynamic> mapEventToState(
    NavigationEvent event,
  ) async* {
    if (event is NavigatorActionPop) {
      yield navigatorKey.currentState.pop;
    } else if (event is NavigateToOnBoardingEvent) {
      yield navigatorKey.currentState
          .pushReplacementNamed(OpenGriyaRoutes.onboarding);
    } else if (event is NavigateToLoginEvent) {
      yield navigatorKey.currentState
          .pushReplacementNamed(OpenGriyaRoutes.login);
    } else if (event is NavigateToRegisterEvent) {
      yield navigatorKey.currentState
          .pushNamed(OpenGriyaRoutes.register);
    } else if (event is NavigateToHomeScreenEvent) {
      yield navigatorKey.currentState.pushReplacementNamed(
        OpenGriyaRoutes.appscreen,
      );

      // admin 
    } else if (event is NavigateToDaftarUserEvent){
      yield navigatorKey.currentState.pushReplacementNamed(
        OpenGriyaRoutes.daftarusers,
      );
    } else if (event is NavigateToDaftarKategoriEvent){
      yield navigatorKey.currentState.pushReplacementNamed(
        OpenGriyaRoutes.daftarkategori,
      );
    }
  }
}
