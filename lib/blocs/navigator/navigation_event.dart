part of 'navigation_bloc.dart';

@immutable
abstract class NavigationEvent extends Equatable {
  const NavigationEvent();

  @override
  List<Object> get props => [];
}

class NavigatorActionPop extends NavigationEvent {}

class NavigateToOnBoardingEvent extends NavigationEvent {}

class NavigateToHomeScreenEvent extends NavigationEvent {}

class NavigateToLoginEvent extends NavigationEvent {}

class NavigateToRegisterEvent extends NavigationEvent {}

class NavigateToAkunEvent extends NavigationEvent {}

class NavigateToEditProfilEvent extends NavigationEvent{}

class NavigateToSettingsEvent extends NavigationEvent{}

class NavigateToChangePasswordEvent extends NavigationEvent{}




// menu admin 
class NavigateToDaftarUserEvent extends NavigationEvent {}
class NavigateToDaftarKategoriEvent extends NavigationEvent {}