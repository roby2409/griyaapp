part of 'registerform_validation_bloc.dart';

@immutable
abstract class RegisterformValidationEvent extends Equatable{
  const RegisterformValidationEvent();

  @override
  List<Object> get props => [];
}

class NamaChanged extends RegisterformValidationEvent {
  final String nama;

  const NamaChanged({@required this.nama});

  @override
  List<Object> get props => [nama];

  @override
  String toString() => 'NamaChanged { email: $nama }';
}

class EmailChanged extends RegisterformValidationEvent {
  final String email;

  const EmailChanged({@required this.email});

  @override
  List<Object> get props => [email];

  @override
  String toString() => 'EmailChanged { email: $email }';
}


class PasswordChanged extends RegisterformValidationEvent {
  final String password;

  const PasswordChanged({@required this.password});

  @override
  List<Object> get props => [password];

  @override
  String toString() => 'PasswordChanged { password: $password }';
}