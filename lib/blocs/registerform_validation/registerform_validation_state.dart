part of 'registerform_validation_bloc.dart';

@immutable
class RegisterformValidationState {
  final bool isInProcessNama;
  final bool isInProcessEmail;
  final bool isInProcessPassword;

  final bool isNamaValidated;
  final bool isEmailValidated;
  final bool isPasswordValidated;

  final String errorNamaMessage;
  final String errorEmailMessage;
  final String errorPasswordMessage;

  bool get isValidForm =>
      errorNamaMessage == null &&
      errorEmailMessage == null &&
      errorPasswordMessage == null;

  RegisterformValidationState(
      {this.isInProcessNama,
      this.isInProcessEmail,
      this.isInProcessPassword,

      this.isNamaValidated,
      this.isEmailValidated,
      this.isPasswordValidated,

      this.errorNamaMessage,
      this.errorEmailMessage,
      this.errorPasswordMessage});

  factory RegisterformValidationState.empty() {
    return RegisterformValidationState(
        isInProcessNama: false,
        isInProcessEmail: false,
        isInProcessPassword: false,

        isNamaValidated: false,
        isEmailValidated: false,
        isPasswordValidated: false);
  }

  RegisterformValidationState copyWith(
      {bool isInProcessNama,
      bool isInProcessEmail,
      bool isInProcessPassword,

      bool isNamaValidated,
      bool isEmailValidated,
      bool isPasswordValidated,

      String errorNamaMessage,
      String errorEmailMessage,
      String errorPasswordMessage}) {
    return RegisterformValidationState(
      isNamaValidated: isNamaValidated ?? this.isNamaValidated,
      isEmailValidated: isEmailValidated ?? this.isEmailValidated,
      isPasswordValidated: isPasswordValidated ?? this.isPasswordValidated,

      isInProcessNama: isInProcessNama ?? this.isInProcessNama,
      isInProcessEmail: isInProcessEmail ?? this.isInProcessEmail,
      isInProcessPassword: isInProcessPassword ?? this.isInProcessPassword,

      errorNamaMessage: errorNamaMessage,
      errorEmailMessage: errorEmailMessage,
      errorPasswordMessage: errorPasswordMessage,
    );
  }

  RegisterformValidationState loadingNama() {
    return this.copyWith(isInProcessNama: true);
  }

  RegisterformValidationState loadingEmail() {
    return this.copyWith(isInProcessEmail: true);
  }

  RegisterformValidationState loadingPassword() {
    return this.copyWith(isInProcessPassword: true);
  }

  RegisterformValidationState validatedNama(
      {@required String errorNamaMessage}) {
    return this
        .copyWith(errorNamaMessage: errorNamaMessage, isInProcessNama: false);
  }

  RegisterformValidationState validatedEmail(
      {@required String errorEmailMessage}) {
    return this.copyWith(
        errorEmailMessage: errorEmailMessage, isInProcessEmail: false);
  }

  RegisterformValidationState validatedPassword(
      {@required String errorPasswordMessage}) {
    return this.copyWith(
        errorPasswordMessage: errorPasswordMessage, isInProcessPassword: false);
  }
}
