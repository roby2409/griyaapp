import 'dart:async';
import 'package:griya/common/app_validator.dart';
import 'package:rxdart/rxdart.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

part 'registerform_validation_event.dart';
part 'registerform_validation_state.dart';

class RegisterformValidationBloc
    extends Bloc<RegisterformValidationEvent, RegisterformValidationState> {
  @override
  RegisterformValidationState get initialState =>
      RegisterformValidationState.empty();

  @override
  Stream<Transition<RegisterformValidationEvent, RegisterformValidationState>>
      transformEvents(
    Stream<RegisterformValidationEvent> events,
    TransitionFunction<RegisterformValidationEvent, RegisterformValidationState>
        transitionFn,
  ) {
    final nonDebounceStream = events.where((event) {
      return (event is! NamaChanged &&
          event is! EmailChanged &&
          event is! PasswordChanged);
    });
    final debounceStream = events.where((event) {
      return (event is NamaChanged ||
          event is EmailChanged ||
          event is PasswordChanged);
    }).debounceTime(Duration(milliseconds: 300));
    return super.transformEvents(
      nonDebounceStream.mergeWith([debounceStream]),
      transitionFn,
    );
  }

  @override
  Stream<RegisterformValidationState> mapEventToState(
    RegisterformValidationEvent event,
  ) async* {
    if (event is NamaChanged) {
      yield this.state.loadingNama();
      String backendValidationMessage =
          await this.simulatedBackendNamaFunctionality(event.nama);
      yield this
          .state
          .validatedNama(errorNamaMessage: backendValidationMessage);
    }

    if (event is EmailChanged) {
      yield this.state.loadingEmail();
      String backendValidationMessage =
          await this.simulatedBackendEmailFunctionality(event.email);
      yield this
          .state
          .validatedEmail(errorEmailMessage: backendValidationMessage);
    }
    if (event is PasswordChanged) {
      yield this.state.loadingPassword();
      String backendValidationMessage =
          await this.simulatedBackendPasswordFunctionality(event.password);
      yield this
          .state
          .validatedPassword(errorPasswordMessage: backendValidationMessage);
    }
  }

  Future<String> simulatedBackendNamaFunctionality(String input) async {
    // This simulates delay of the backend call
    await Future.delayed(Duration(milliseconds: 500));
    // This simulates the return of the backend call
    return Validators.valueExists(input);
  }

  Future<String> simulatedBackendEmailFunctionality(String input) async {
    // This simulates delay of the backend call
    await Future.delayed(Duration(milliseconds: 500));
    // This simulates the return of the backend call
    return Validators.validateEmail(input);
  }

  Future<String> simulatedBackendPasswordFunctionality(String input) async {
    // This simulates delay of the backend call
    await Future.delayed(Duration(milliseconds: 500));
    // This simulates the return of the backend call
    return Validators.valueExists(input);
  }
}
