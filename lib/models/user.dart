// To parse this JSON data, do
//
//     final user = userFromJson(jsonString);

import 'dart:convert';

List<User> userListFromJson(String str) => List<User>.from(json.decode(str).map((x) => User.fromJson(x)));

User userFromJson(String str) => User.fromJson(json.decode(str));

String userToJson(User data) => json.encode(data.toJson());

String userLoginToJson(UserLogin userLogin) =>
    json.encode(userLogin.toDatabaseJson());

class User {
  User({
    this.id,
    this.level,
    this.email,
    this.token,
    this.expireAt,
  });

  int id;
  int level;
  String email;
  String token;
  int expireAt;

  factory User.fromJson(Map<String, dynamic> json) => User(
        id: json["id"],
        level: json["level"],
        email: json["email"],
        token: json["token"],
        expireAt: json["expireAt"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "level": level,
        "email": email,
        "token": token,
        "expireAt": expireAt,
      };
}

class UserLogin {
  String email;
  String password;

  UserLogin({this.email, this.password});

  Map<String, dynamic> toDatabaseJson() =>
      {"email": this.email, "password": this.password};
}
