import 'dart:convert';

MessageApi messageApiFromJson(String str) =>
    MessageApi.fromJson(json.decode(str));

class MessageApi {
  MessageApi({
    this.message,
    this.status,
    this.data,
  });

  String message;
  int status;
  dynamic data;

  factory MessageApi.fromJson(Map<String, dynamic> json) =>
      MessageApi(
        message: json["message"],
        status: json["status"],
        data: json["data"],
      );

  Map<String, dynamic> toJson() => {
        "message": message,
        "status": status,
        "data": data,
      };
}

