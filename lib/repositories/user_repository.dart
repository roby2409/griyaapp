import 'dart:async';
import 'package:griya/databases/database.dart';
import 'package:griya/models/user.dart';
import 'package:griya/providers/providers.dart';
import 'package:meta/meta.dart';

class UserRepository {
  final dbHelper = DatabaseHelper();
  ApiProvider _apiProvider = ApiProvider();

  Future<User> authenticate({
    @required String email,
    @required String password,
  }) async {
    UserLogin userLogin = UserLogin(email: email, password: password);
    final response =
        await _apiProvider.post("auth/login", userLoginToJson(userLogin));
    final result = userFromJson(response);
    User user = User(
        id: 0,
        level: result.level,
        email: result.email,
        token: result.token,
        expireAt: result.expireAt);
    return user;
  }

  Future<void> deleteToken({@required int id}) async {
    await dbHelper.deleteUser(id);
  }

  Future<void> persistToken({@required User user}) async {
    // write token with the user to the database
    await dbHelper.createUser(user);
  }

  Future<bool> hasToken() async {
    bool result = await dbHelper.checkUser(0);
    return result;
  }

  Future<User> notExpired(String bearerToken) async {
    final response = await _apiProvider.get("CheckExpiredToken", bearerToken);
    final result = userFromJson(response);
    User user = User(
        id: 0,
        level: result.level,
        email: result.email,
        token: result.token,
        expireAt: result.expireAt);
    return user;
  }

  Future<User> getUser() async {
    return (await dbHelper.getUser(0));
  }


  Future<List<User>> fetchUserList() async {
    ApiProvider _apiProvider = ApiProvider();
    final response = await _apiProvider.get("user");
    final semuaUser = userListFromJson(response);
    return semuaUser;
  }
}
