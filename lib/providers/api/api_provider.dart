import 'dart:io';
import 'dart:async';
import 'package:griya/common/app_exception.dart';
import 'package:griya/models/message_api.dart';
import 'package:http/http.dart' as http;

class ApiProvider {
  final String _baseUrl = "http://192.168.0.17/griya/public/";
  // final String _baseUrl = "https://apigriya.pppkpusri.com/public/";

  Future<dynamic> get(String url, [bearerToken]) async {
    var responseJson;
    try {
      final response = await http.get(_baseUrl + url, headers: {
        HttpHeaders.contentTypeHeader: "application/json",
        HttpHeaders.authorizationHeader: "Bearer $bearerToken"
      });
      responseJson = _returnResponse(response);
    } on SocketException {
      print('No net');
      throw FetchDataException('Tidak bisa terhubung ke server');
    } on HandshakeException {
      throw FetchDataException('Koneksi internet terlalu lambat');
    } on HttpException {
      throw FetchDataException('Internet terputus');
    }
    print('api get recieved!');
    return responseJson;
  }

  Future<dynamic> post(String url, dynamic body, [bearerToken]) async {
    var responseJson;
    try {
      final response = await http.post(_baseUrl + url, body: body, headers: {
        HttpHeaders.contentTypeHeader: "application/json",
        HttpHeaders.authorizationHeader: "Bearer $bearerToken"
      });
      responseJson = _returnResponse(response);
    } on SocketException {
      print('No net');
      throw FetchDataException('Tidak bisa terhubung ke server');
    } on HandshakeException {
      throw FetchDataException('Koneksi internet terlalu lambat');
    } on HttpException {
      throw FetchDataException('Internet terputus');
    }
    print('api post.');
    return responseJson;
  }

  Future<dynamic> put(String url, dynamic body) async {
    print('Api Put, url $url');
    var responseJson;
    try {
      final response = await http.put(_baseUrl + url, body: body);
      responseJson = _returnResponse(response);
    } on SocketException {
      print('No net');
      throw FetchDataException('Tidak bisa terhubung ke server');
    } on HandshakeException {
      throw FetchDataException('Koneksi internet terlalu lambat');
    } on HttpException {
      throw FetchDataException('Internet terputus');
    }
    print('api put.');
    return responseJson;
  }

  Future<dynamic> delete(String url) async {
    print('Api delete, url $url');
    var apiResponse;
    try {
      final response = await http.delete(_baseUrl + url);
      apiResponse = _returnResponse(response);
    } on SocketException {
      print('No net');
      throw FetchDataException('Tidak bisa terhubung ke server');
    } on HandshakeException {
      throw FetchDataException('Koneksi internet terlalu lambat');
    } on HttpException {
      throw FetchDataException('Internet terputus');
    }
    print('api delete.');
    return apiResponse;
  }

  dynamic _returnResponse(http.Response response) async {
    switch (response.statusCode) {
      case 200:
        var responseJson = response.body;
        print(responseJson);
        return responseJson;
      case 400: // query gagal
        final result = messageApiFromJson(response.body.toString());
        throw BadRequestException(result.message);
      case 401: // session habis
        final result = messageApiFromJson(response.body.toString());
        throw UnauthorisedException(result.message);
      case 403: // not found id
        final result = messageApiFromJson(response.body.toString());
        throw InvalidInputException(result.message);
      case 500:
      default:
        // throw FetchDataException(
        //     'Terjadi kesalahan dengan server ${response.statusCode}');
        throw FetchDataException('Terjadi kesalahan dengan server');
    }
  }
}
