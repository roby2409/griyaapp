import 'package:griya/models/user.dart';
import 'package:griya/providers/providers.dart';

class DatabaseHelper {
  final dbProvider = UserProvider.dbProvider;

  Future<int> createUser(User user) async {
    final db = await dbProvider.database;
    var result = db.insert(userTable, user.toJson());
    return result;
  }

  Future<int> deleteUser(int id) async {
    final db = await dbProvider.database;
    var result = await db.delete(userTable, where: "id = ?", whereArgs: [id]);
    return result;
  }

  Future<bool> checkUser(int id) async {
    final db = await dbProvider.database;
    try {
      List<Map> users =
          await db.query(userTable, where: 'id = ?', whereArgs: [id]);
      if (users.length > 0) {
        return true;
      } else {
        return false;
      }
    } catch (error) {
      return false;
    }
  }

  Future<User> getUser(int id) async {
    final db = await dbProvider.database;
    var res = await db.query(userTable, where: 'id = ?', whereArgs: [id]);

    User user = res.isNotEmpty ? User.fromJson(res.first) : null;
    return user;
  }
}
